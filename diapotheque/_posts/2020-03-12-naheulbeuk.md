---
layout: post
title:  "Naheulbeuk"
date:   2020-03-12 08:22:00 +0100
tags: nain ogre nyctalope elfe
author: Kévin
---


### <Musique>


> Ambiance médiévale calme.,

### Le Ranger


> Et merde, on se les gèle ici... Tiens, voilà quelqu'un.,

### Le Ranger


> Salut, tu viens pour l'aventure ?,

### Le Nain


> Hé ouais ! Je suis le Nain !,


split_slide_there


### Le Ranger


> Ca se voit.,

### Le Nain


> Et ça, c'est le donjon ?,

### Le Ranger


> Effectivement. C'est le donjon de Naheulbeuk.,

### Le Nain


> Il a pas l'air terrible !,

### Le Ranger


> Faut pas s'y fier, car personne n'en est ressorti !,

### Le Nain


> Ah bon ?,

### Le Ranger


> Faut dire aussi que personne y est entré.,

### L'Elfe


> Bonjour, bonjour !,

split_slide_there



### Le Ranger


> Ah ! Voilà l'Elfe !,

### L'Elfe


> Et oui c'est moi, mais qui est ce petit personnage ?,

### Le Nain


> Je suis un Nain, connasse !,

### L'Elfe


> Un Nain ? Mais quelle horreur !,

### Le Nain


> Mais ta gueule...,

### Le Ranger


> S'il vous plaît, ne commencez pas à vous battre !,

### <Baston Orale>


> ,


split_slide_there



### 	L'Elfe


> Mais ouais ! Tu fais chier ...,

### 	Le Nain


> Mais ouais c'est ça... allez...,

### 	L'Elfe


> Non mais vraiment t'es... t'es trop con toi, allez...,

### 	Le Nain


> Ouais, ouais, ouais c'est ça, ta gueule !,

### 	L'Elfe


> T'es... t'es stupide !,

### 	Le Nain


> Tu fais chier, merde !,

### Le Ranger


> Et voilà, c'est déjà le bordel ! VOS GUEULES !,

### Le Ranger


> Y'a un mec bizarre qui s'approche.,

### Le Voleur


> Salut à vous, belle compagnie. Vous m'attendiez ?,

### Le Ranger


> Tu viens pour le donjon ?,

### Le Voleur


split_slide_there



> Certes. Je suis le Voleur.,

### L'Elfe


> Oh, un monstre !,

### L'Ogre


> Gotfeurdom !,

### La Magicienne


> Salut ! Kof kofff...,

### Le Ranger


> Ah, mais qui voilà donc ?,

### La Magicienne


> Je suis la Magicienne, et voilà l'Ogre.,

### L'Ogre


> Zog zog.,

### Le Nain


> Mais on comprend rien quand il parle !,

### La Magicienne


> Je vous ferai la traduction.,

### Le Ranger


> J'espère bien !,

### Le Barbare


> Salut !,


split_slide_there



### Le Voleur


> Tiens donc, un Paysan !,

### Le Barbare


> Chuis un Barbare !,

### Le Voleur


> Aucune différence !,

### Barbare


> Weuah !,

### <Bruit>


> Le Barbare frappe le Voleur.,

### Le Ranger


> Je crois que c'est ça la différence.,

### Le Voleur


> Mais il est con...,

### Le Barbare


> Tarlouze !,

### <Bruit>


> Le Barbare donne une baffe au Voleur.,

### Le Ranger


> Bon alors est-ce que vous avez bien tous votre équipement ?,

### Le Nain


split_slide_there


> Mais bien sûr qu'on a notre équipement !,

### Le Ranger


> Est-ce que vous avez des torches...,

### Le Nain


> Mais bien sûr qu'on a des torches !,

### Le Ranger


> De quoi manger...,

### Le Nain


> Mais ouais, on a pris à manger !,

### Le Ranger


> Est-ce que vous avez à boire et...,

### Le Nain


> Mais bien sûr qu'on a de la boisson !,

### Le Ranger


> Est-ce que vous avez vos armes et...,

### Le Nain


> Mais bien sûr qu'on a des armes !,

### Le Ranger


split_slide_there


> Mais tu va la fermer oui ?!,

### <Bruit>


> Le Ranger frappe le Nain.,

### Le Ranger


> Mais quel chiant ce Nain !,

### L'Ogre


> Akala volo.,

### Le Ranger


> Qu'est-ce qu'il a ?,

### La Magicienne


> Il a envie de chier.,

### Le Ranger


> Ah, c'est malin, il aurait pu faire à l'auberge !,

### Le Ranger


> Bon vas-y on t'attend.,

### La Magicienne


> Broudaf zog zog.,

### L'Ogre


> Doula.,

### Le Ranger

split_slide_there

> Halala...,

### La Magicienne


> Les Ogres sont sensibles...,

### Le Ranger


> Rien à foutre !,

### La Magicienne


> Les Ogres sont parfois poètes...,

### Le Ranger


> Ca nous intéresse pas !,

### <Bruit>


> L'Ogre défèque près du donjon.,

### L'Elfe


> Mais il pourrait aller plus loin quand même !,

### La Magicienne


> Les Ogres peuvent chanter et danser...,

### Le Ranger


> J'ai dit ça nous intéresse pas !,

### La Magicienne


> Un Ogre peut faire la cuisine ou les papiers peints...,

### Le Ranger


> Tu veux vraiment mon poing sur la gueule ?,

### La Magicienne


> Et ben puisque c'est comme ça, restez incultes !,

### L'Ogre


> Huh huh huh huh huh...,

### Le Nain


> J'aimerai bien rentrer dans le donjon, j'ai froid !,

### L'Ogre


> Akala miammiam.,

### Le Ranger


> Qu'est-ce qu'il dit ?,


split_slide_there



### La Magicienne


> Il dit qu'il a faim.,

### Le Ranger


> Mais on a mangé y a deux heures !,

### L'Elfe


> C'est vraiment un ventre, cet Ogre !,

### Le Ranger


> Bon, l'Ogre peut toujours manger un sandwich avant d'entrer,,

###             mais qu'il se dépêche.


> undefined,

### La Magicienne


> Zog-zog akiita.,

### L'Ogre


> Doula.,

### Le Voleur


> Au fait, quel est le but de notre mission ?,

### <Bruit>



> Le Ranger déplie un parchemin.,


split_slide_there


### Le Ranger


> Nous devons retrouver la douzième statuette de Gladeulfeurha...,

### L'Elfe


> Une statuette ?,

### Le Ranger


> Il est écrit dans les tablettes de Skeloss que seul un Gnome des Forêts du Nord unijambiste dansant à la pleine lune au milieu des douze statuettes enroulées dans du jambon ouvrira la porte de Zaral Bak et permettra l'accomplissement de la prophétie.,

### Le Voleur


> Mais quelle est donc cette étrange prophétie ?,

### Le Ranger


> Aucune idée, c'est la fortune qui nous intéresse.,

### L'Elfe


> C'est pas grave, c'est toujours bien une prophétie.,

### Le Ranger


> Ca dépend, des fois ça parle de détruire le monde.,

### Le Barbare


> On entre ?!!,

### L'Ogre


> Glozou bok.,


### Le Nain



split_slide_there


> Qu'est-ce qu'il dit ?,

### La Magicienne


> Il a terminé son sandwich.,

### Le Voleur


> Nous devons trouver une solution pour entrer dans ce donjon...,

### L'Ogre


> Akala glouglou.,

### Le Ranger


> Qu'est-ce qu'il a encore, l'ahuri ?,

### La Magicienne


> Il dit qu'il a soif maintenant.,

### Le Ranger


> Mais qu'il boive, et qu'il fasse pas chier !,

### <Bruit>


> L'Ogre débouche une bouteille et commence à boire.,

### Le Nain


> Ah j'en ai marre, il fait froid, j'me barre !,

### L'Elfe


> Bon débarras !,

### L'Ogre


> Burp !,

### Le Ranger


> Hé le Nain ! Reste avec nous sinon tu vas faire plaisir à l'Elfe !,

### Le Nain


> Ha, merde.,

### Le Nain


> T'as raison ! Rien que pour l'emmerder, j'vais rester !,

### L'Elfe


> Rien à foutre, moi je vais ouvrir cette porte...,

### <Bruit>


> L'Elfe frappe à la porte.,

### Le Ranger


> Mais qu'est-ce que tu fais ?,

### L'Elfe


> Et bien, je frappe pour qu'on vienne nous ouvrir.,

### Le Ranger


> Bravo, ça va être discret comme entrée !,

### <Bruit>



> Le groupe applaudit.,


split_slide_there



### Le Nain


> Mais quelle conne !,

### <Bruit>


> La porte s'ouvre.,

### L'Elfe


> Vous voyez, ça marche.,

### Le Barbare


> Hmf !