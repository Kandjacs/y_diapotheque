
let page = 0

// Function to split the .md in an array
function split(){
  let content = document.getElementsByClassName("slides")
  let mdContent = content[0].innerHTML
  let slides = mdContent.split('split_slide_there')


  return slides
}

//  Function to display the indivudual slides
function displaySlide(){
  let slides = split()
  let slide = document.getElementById("slide")

  slide.innerHTML = slides[page]
}


function paginator(){
  let slides = split()

  // Next Page Paginator
  let after = document.getElementById("after")
  after.addEventListener('click', nextPage)

  function nextPage(){
    if (page >= slides.length - 1){
      return page
    } else  page++
    displaySlide()
  }

  // Previous Page Paginator
  let before = document.getElementById("before")
  before.addEventListener('click', prevPage)

  function prevPage(){
    if (page <= 0){
      return page
    } else page--
    displaySlide()
  }
}

displaySlide()
paginator()

